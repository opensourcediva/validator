<?php

class Validator{
		function validate($post, $map_vt){
			$check = 0;	
			//echo 'check bf: '.$check ; echo '<br>';
			// $post = array('email' => 'test@test.ca');
			// $map_vt = array('email' => 'required:email');
			//print_r($map_vt);
			
			foreach($map_vt as $k =>$v){
				$field_check = explode(":",$v);
				//echo '<br>'.$field_check[0].'  '.$field_check[1].'<br>';
				if($field_check[0] == "required"){
					if(empty($v)){ 
						$check++;
					}
				}
				
				if(!empty($post[$k])){	
						if($field_check[1] == "email"){
							if($this->valid_email($post[$k]) == false){
									//echo 'email not good!'; echo '<br>';
								$check++;
							}
						}			
						else if($field_check[1] == "date"){
							if($this->check_date_range($post[$k]) == false){
								$check++;
							}
						}
						else if($field_check[1] == "number"){
							if(!is_numeric($post[$k]))	
							$check++;
						}
						else if($field_check[1] == "alphanumeric"){
							if(!ctype_alnum($post[$k]))	
							$check++;
						}
						else if($field_check[1] == "alpha"){
							if(!ctype_alpha(str_replace(array(".","-"," "),"",$post[$k])))	
							$check++;
						}
				//echo 'check After: '.$check .'   '.$k; echo '<br>';	
				}
	
			}	
			
			
				
			if($check == 0)
				return true;
			else 
				return false;
	}
	 function check_date_range($date_from_user){
          // Convert to timestamp
         $start_ts = strtotime("today"); 
         $end_ts = strtotime("+10 years");
         $user_ts = strtotime($date_from_user);
		
		//echo '<br>'.$start_ts.'<br>'. $user_ts;
          // Check that user date is between start & end
          if ($user_ts >= $start_ts )
              return true;
          else
              return false;
      }
        
	 function valid_email($email){
            $regexp = "/[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9]){1,}?/";
                if (preg_match($regexp, $email)) {
                    return true;
                } else {
                    return false;
                }
      }
	
}

	 
?>
